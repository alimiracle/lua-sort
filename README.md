# lua-sort:

a Lua module that provides implementations of various sorting algorithms.

## Usage:

To use the `lua-sort` module, you can require it in your Lua program:

```lua
local sort = require("lua-sort")
```

Once required, you can use any of the provided sorting algorithms by calling the corresponding function on the `sort` module.

For example, to sort an array using the bubble sort algorithm:

```lua
local arr = {5, 2, 9, 1, 7}
sort.bubbleSort(arr)
```

After calling the sorting function, the `arr` table will be sorted in ascending order.

You can replace `bubbleSort` with the name of any other sorting algorithm to use a different sorting method.

## Available Sorting Algorithms:

- Bubble Sort algorithm
- Insertion Sort algorithm
- Selection Sort algorithm
- Merge Sort algorithm
- Quick Sort algorithm
- Shell Sort algorithm
- Counting Sort algorithm
- Heap Sort algorithm
- Radix Sort algorithm
- Introsort algorithm
- Block Sort algorithm
- Timsort algorithm
- Cubesort algorithm
- Exchange Sort algorithm
- Tree Sort algorithm
- Cycle Sort algorithm
- Strand Sort algorithm
- Tournament Sort algorithm
- Gnome Sort algorithm
- Comb Sort algorithm
- Cocktail Shaker Sort algorithm
- Pigeonhole Sort algorithm
- Bucket Sort algorithm (uniform keys)
- Bucket Sort algorithm (integer keys)
- Spreadsort algorithm
- Burstsort algorithm
- Flashsort algorithm

## License:

This project is licensed under the GPL v3 License - see the LICENSE file for details.